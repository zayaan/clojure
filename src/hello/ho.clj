;;
;; Higher order functions
;;
(ns hello.ho
  (:require [clojure.test :refer :all]))

(defn add [x y]
  (+ x y))

(defn plus-10 [x]
  (+ x 10))

(defn mapz [f xs]
  (if (empty? xs)
    ()
    (cons (f (first xs)) (mapz f (rest xs)))))

(defn sum-list [xs]
  (if (empty? xs)
    0
    (+ (first xs) (sum-list (rest xs)))))

;; The problem with this function is that it does not work for 
;; empty list.
(defn max-list [xs]
  (if (empty? xs) 
    0
    (if (> (first xs) (max-list (rest xs)))
      (first xs) 
      (max-list (rest xs)))))

(defn max-list-iter [xs]
  (letfn [(iter [max xs]
            (if (empty? xs)
              max
              (let [new-max (if (> max (first xs)) max (first xs))]
                (recur new-max (rest xs)))))]
    (iter (first xs) (rest xs))))

(deftest test-max-list-iter
  (is (nil? (max-list-iter '())))
  (is (= (max-list-iter '(7 6 9 1)) 9)))

(defn takez [n xs]
  (letfn [(iter [n xs res]
            (if (empty? xs)
              (reverse res)
              (if (= n 0)
                (reverse res)
                (recur (- n 1) (rest xs) (cons (first xs) res)))))]
          (iter n xs ())))

(deftest test-takez
  (is (= (takez 2 '(1 2 3 4)) '(1 2)))
  (is (= (takez 5 (range 1 4)) '(1 2 3)))
  (is (= (takez 0 (range 1 4)) '())))

(defn multiples [n m]
  (letfn [(iter [counter res]
            (if (= m counter)
              (reverse res)
              (recur (+ counter 1) (cons (* n counter) res))))]
    (iter 1 ())))

(defn factors [n] =
  (letfn [(iter [counter res]
            (if (< (int (/ n 2)) counter)
              (reverse res)
              (if (= (mod n counter) 0) 
              (recur (+ counter 1) (cons counter res))
               (recur (+ counter 1) res))))]
    (iter 1 ())))

(deftest test-factors
  (is (= (factors 0) '()))
  (is (= (factors 32) '(1,2,4,8,16))))
  
(defn say-barbarianking [persoName]
 (str "THE Barbarian King" personName))
 
